import * as express from "express";
import errorHandler from "./middleware/ErrorHandler";

//Initializing Express app
const app = express();

//body Parser setup
app.use(express.json());

app.get(
  "/",
  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.status(200).json({ message: "Welcome" });
  }
);

//Error Handler Setup
app.use(errorHandler.notFoundHandler);
app.use(errorHandler.defaultErrorHandler);

export default app;

import { NextFunction, Request, Response } from "express";
import * as createError from "http-errors";

class ErrorHandler {
  notFoundHandler(req: Request, res: Response, next: NextFunction) {
    next(createError(404, "Your requested content was not found!"));
  }

  defaultErrorHandler(
    err: createError.HttpError,
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    res.status(err.status || 500).json(err);
  }
}

export default new ErrorHandler();
